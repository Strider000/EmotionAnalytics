﻿using System;
using Xamarin.Forms;

namespace EmoSong
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

		async void btn_Clicked(object sender, EventArgs e)
		{
			try
			{
				String Image = await Utilities.getCamera();
				String res = await App.RManager.ReadEmotion(Convert.FromBase64String(Image));

				await DisplayAlert("Emotion Detected", res, "OK");
			}
			catch (Exception ex) { }
		}
	}
}
